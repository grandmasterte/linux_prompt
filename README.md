# Default Prompt
This is the default prompt that shipped with my Linux distribution.

## Preview

Here is what the prompt looks like while outside a git repository:

![Default Prompt][prompt]

And here is what the prompt looks like while inside a git repository:

![Git Prompt][git_prompt]

[prompt]: pics/base_prompt.png "Default Prompt"
[git_prompt]: pics/git_prompt.png "Git Prompt"